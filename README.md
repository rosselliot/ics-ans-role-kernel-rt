# ics-ans-role-kernel-rt

Ansible role to install real time kernel and enable dkms.

## Role Variables

```yaml
kernel_rt_version: 3.10.0-1062.12.1.rt56.1042.el7
kernel_rt_boot_parameters:
  - idle=poll
  - intel_idle.max_cstate=0
  - processor.max_cstate=1
  - skew_tick=1
  - clocksource=tsc
kernel_rt_isolated_cores: "0"
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-kernel-rt
```

## License

BSD 2-clause
